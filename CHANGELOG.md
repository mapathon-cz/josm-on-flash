# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog][] and this project adheres to
[Semantic Versioning][].

[Keep a Changelog]: http://keepachangelog.com/
[Semantic Versioning]: http://semver.org/

## [Unreleased][]
This repository is deprecated in favour of
https://gitlab.com/mapathon-cz/josm-on-usb-stick.

## [0.2.2][] - 2020-02-20
### Fixed
- Revision comparison is script.

## [0.2.1][] - 2020-02-20
### Added
- Java msi installers.

## [0.2.0][] - 2020-02-20
### Changed
- Split GitLab CI/CD to 2 stages -- build `josm-on-flash.zip` file if there is
  new JOSM version. If success, build GitLab page, too.

## [0.1.3][] - 2020-02-18
### Fixed
- Missing GitLab CI/CD package.

## [0.1.2][] - 2020-02-18
### Fixed
- Missing packages in GitLab CI/CD.

## [0.1.1][] - 2020-02-18
### Fixed
- Gitlab CI/CD syntax error.

## 0.1.0 - 2020-02-18
### Added
- Changelog, license, readme.
- Script to generate josm on flash folder.
- GitLab CI/CD, GitLab pages.

[Unreleased]: https://gitlab.com/mapathon-cz/josm-on-flash/compare/v0.2.2...master
[0.2.2]: https://gitlab.com/mapathon-cz/josm-on-flash/compare/v0.2.1...v0.2.2
[0.2.1]: https://gitlab.com/mapathon-cz/josm-on-flash/compare/v0.2.0...v0.2.1
[0.2.0]: https://gitlab.com/mapathon-cz/josm-on-flash/compare/v0.1.3...v0.2.0
[0.1.3]: https://gitlab.com/mapathon-cz/josm-on-flash/compare/v0.1.2...v0.1.3
[0.1.2]: https://gitlab.com/mapathon-cz/josm-on-flash/compare/v0.1.1...v0.1.2
[0.1.1]: https://gitlab.com/mapathon-cz/josm-on-flash/compare/v0.1.0...v0.1.1
