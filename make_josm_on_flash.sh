#!/bin/bash

function josm_version
{
        # get josm
        wget https://josm.openstreetmap.de/josm-tested.jar

        # get version
        jar -xf josm-tested.jar
        grep Revision REVISION | cut -d' ' -f2 > josm_revision
}

function download_java
{
        wget https://github.com/AdoptOpenJDK/openjdk13-binaries/releases/download/jdk-13.0.2%2B8/OpenJDK13U-jre_x86-32_windows_hotspot_13.0.2_8.msi
        wget https://github.com/AdoptOpenJDK/openjdk13-binaries/releases/download/jdk-13.0.2%2B8/OpenJDK13U-jre_x64_windows_hotspot_13.0.2_8.msi
        wget https://download.java.net/java/GA/jdk13.0.2/d4173c853231432d94f001e99d882ca7/8/GPL/openjdk-13.0.2_windows-x64_bin.zip
        unzip openjdk-13.0.2_windows-x64_bin.zip
}

function download_revision
{
        touch revision
        wget -O revision https://mapathon-cz.gitlab.io/josm-on-flash/revision
}

jv=$(josm_version && cat josm_revision)
cv=$(download_revision && cat revision)

if [ x"$jv" != x"$cv" ]
then
        mkdir josm-on-flash
        mv josm-tested.jar josm-on-flash/
        mv runjosm.bat josm-on-flash/

        download_java
        mv jdk-13.0.2/ josm-on-flash/
        mv OpenJDK13U-jre_x86-32_windows_hotspot_13.0.2_8.msi josm-on-flash/
        mv OpenJDK13U-jre_x64_windows_hotspot_13.0.2_8.msi josm-on-flash/

        zip -9 -r josm-on-flash.zip josm-on-flash

        mkdir public
        mv josm-on-flash.zip public/
        mv josm_revision public/revision

        exit 0
fi

exit 1
